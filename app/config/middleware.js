module.exports = {
    load: {
      before: ["responseTime", "logger", "cors", "responses", "gzip",],
      after: ["parser", "router", "timer"],
    },
    settings: {
      timer: {
        enabled: true,
      },
      getEmail: {
        enabled: true,
      },
    },
  };