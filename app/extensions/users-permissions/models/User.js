'use strict';

function gen_password(len) {
    let password = "";
    let symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!№;%:?*()_+=";
    for (let i = 0; i < len; i++){
        password += symbols.charAt(Math.floor(Math.random() * symbols.length));     
    }
    return password;
}

module.exports = {
   lifecycles: {
    async afterCreate(data, model) {
        
        const genDarkPass = gen_password(7);
        const curUserId = data.id;

        await strapi.query("dark-password").create({
            DarkPass: genDarkPass,
            users_permissions_user: curUserId,

        });
    },
  },
};