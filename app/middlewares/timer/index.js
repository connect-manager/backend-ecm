const { sanitizeEntity } = require('strapi-utils');

async function CheckFailConnection(query) {
    const data = await strapi.query('user', 'users-permissions').findOne(query);
    return data.failCounter; 
}
async function blockedCurUser(query) {
  const changeParamBlock =  await strapi.query('user', 'users-permissions').model.find({
    username: query.username }).update({ 
      blocked: true,
      failCounter: 0,
     });
    return changeParamBlock;
};



module.exports = strapi => {
    return {
      initialize() {
        
        strapi.app.use(async (ctx, next) => {
          const start = Date.now();
          if (ctx.url == "/EvmController/authentication") {
            
            const params = ctx.request.body;
            const query = { };	
            query.username = params.identifier;

            const failCounter = await CheckFailConnection(query)
            const response = failCounter > 5 ? await blockedCurUser(query) : '';
            
          }  
            await next();
            
            const delta = Math.ceil(Date.now() - start);
            
            ctx.set('X-Response-Time', delta + 'ms');
        });
      },
    };
  };