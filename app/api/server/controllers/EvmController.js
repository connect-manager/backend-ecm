const { sanitizeEntity } = require('strapi-utils');
const { ClearFailCounter, CheckValidationPassword, FindUser, GetDataForUser, JsonResponse } = require('../services/EvmService')

module.exports = {
    async authentication(ctx) {
        const emailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const provider = ctx.params.provider || 'local';
        const params = ctx.request.body;
        const query = { provider };		
        const isEmail = emailRegExp.test(params.identifier);

        if (isEmail) {
            query.email = params.identifier.toLowerCase();
        } else {
            query.username = params.identifier;
        }
        
        const user = await FindUser(query);
        const validPassword = await CheckValidationPassword(query, params.password, user);
        const data = await GetDataForUser(query);
        const response = await JsonResponse(data);
        
        if(user && validPassword && !data.blocked) {
            ClearFailCounter(query)
            await ctx.send(response);
        }
        else {
            await ctx.send('Forbidden', 403)
        }
        
    }
}