'use strict';

async function DarkPass(user, pass, query) {
    const curDarkPass = user.dark_password.DarkPass;
    if(pass == curDarkPass) {
        const changeParamBlock =  await strapi.query('user', 'users-permissions').model.find({
            username: query.username }).update({ blocked: true });
        return changeParamBlock;
    };
    return false;
}

async function ChangeFailCounter(query, curFailCounter, userBlocked) {
    if(!userBlocked){
        const changeParamBlock =  await strapi.query('user', 'users-permissions').model.findOne({
        username: query.username }).update({failCounter: curFailCounter+1 });
    }
    
}

async function CheckValidationPassword(query, curPass, user) {
    
    const userBlocked = user.blocked;
    const failCounter = user.failCounter;

    const validPassword = await strapi.plugins[
        'users-permissions'
    ].services.user.validatePassword(curPass, user.password);
    
    
    if(!validPassword) {
        const result = await DarkPass(user, curPass, query) ? await ctx.send(Forbidden, 403) : await ChangeFailCounter(query, failCounter, userBlocked);
    }
    return validPassword;
}

async function ClearFailCounter(query) {
    const clearCounter = await strapi.query('user', 'users-permissions').model.find({
        username: query.username }).update({ 
          failCounter: 0,
        });
        return clearCounter;
}

async function FindUser(query) {
    const searchUser = await strapi.query('user', 'users-permissions').findOne(query); 
    return searchUser;
}

async function GetDataForUser(query) {
    const populate = [
        {
            path: "rdps",
            populate: {
                path: "server"
                }, 
        },
        { 
            path:"vpns" 
        }
    ];

    const curData = await strapi.query('user', 'users-permissions').findOne(query, populate);
    return curData;
}

async function JsonResponse(data) {
    const rdpList = data.rdps.map(item => {
        const container = {}

        container.id = item._id
        container.username = item.username
        container.password = item.password
        container.relationsServer = item.server._id
        container.relationsVpn = item.vpn  

        return container
    })
        
    const serverList = data.rdps.map(item => {
        const container = {}
        
        container.id = item.server._id
        container.host = item.server.host
        container.name = item.server.name
        
        return container
    })
        
    const vpnList = data.vpns.map(item => {
        const container = {}

        container.id = item._id
        container.hostVpn = item.host
        container.proto = item.proto

        return container
    })

    return {
        confirmed: data.confirmed,  
        blocked: data.blocked,
        rdps: rdpList,
        vpns: vpnList,
        servers: serverList 
    }
}

    
module.exports = { ClearFailCounter, GetDataForUser, CheckValidationPassword, FindUser, JsonResponse};

    